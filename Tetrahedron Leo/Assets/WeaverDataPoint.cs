using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR 
using UnityEditor;
#endif

[ExecuteInEditMode]
public class WeaverDataPoint : MonoBehaviour
{
    public NodeTypes NodeType;
    public WeaverConnectionSetting[] Connections;
    [SerializeField] private WeaverConnection connectionPrefab;
    [SerializeField] private Color baseColor;
    private Color color;
    [SerializeField] private string label;
    [SerializeField] private Renderer sphereRenderer;
    [SerializeField] private TextMesh textMesh;



    void Start()
    {
        textMesh.text = label;
        color = Color.Lerp(GenerateColorFromLabel(label), baseColor, 0.5f);

#if UNITY_EDITOR
        if (EditorApplication.isPlaying)
        {
#endif

            sphereRenderer.material.color = color;

            foreach (WeaverConnectionSetting point in Connections)
            {
                WeaverConnection connection = Instantiate<WeaverConnection>(connectionPrefab);
                connection.color = color;
                connection.endColor = point.Destination.color;

                connection.From = this;
                connection.To = point.Destination;
                connection.text = point.Prefix + ":" + NodeType.ToString() + "_" + point.ConnectionType.ToString() + "_" + point.Destination.NodeType.ToString(); ;
            }
#if UNITY_EDITOR
        }
#endif
    }

    public void SetLabel(string label)
    {
        this.label = label;
        color = Color.Lerp(GenerateColorFromLabel(label), baseColor, 0.5f);
        textMesh.text = label;
    }

    public Color GenerateColorFromLabel(string label)
    {
        Color color = new Color();

        int r = 0;
        int g = 0;
        int b = 0;

        for (int i = 0; i < label.Length; i++)
            r += (byte)label[i];

        g = r;

        for (int i = 0; i < label.Length; i++)
            g += (byte)label[i];

        b = g;

        for (int i = 0; i < label.Length; i++)
            b += (byte)label[i];

        color.r = (float)(r % 255) / 255.0f;
        color.g = (float)(g % 255) / 255.0f;
        color.b = (float)(b % 255) / 255.0f;
        color.a = 1;

        Debug.Log(color.r + ", " + color.g + ", " + color.g);

        return color;
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(Camera.main.transform, Camera.main.transform.up);
#if UNITY_EDITOR
        textMesh.text = label;
        gameObject.name = label;
        if (EditorApplication.isPlaying)
        {
#endif
        
#if UNITY_EDITOR
        }
        else
        {

            color = Color.Lerp(GenerateColorFromLabel(label), baseColor, 0.5f);
            foreach (WeaverConnectionSetting point in Connections)
            {
                Debug.DrawLine(transform.position, point.Destination.transform.position, color);
                Debug.DrawLine(transform.position + Vector3.up, point.Destination.transform.position, color);
                Debug.DrawLine(transform.position + Vector3.down, point.Destination.transform.position, color);

                point.Text = point.Prefix + ":" + NodeType.ToString() + "_" + point.ConnectionType.ToString() + "_" + point.Destination.NodeType.ToString(); ;
            }
        }
#endif
    }
}

[System.Serializable]
public class WeaverConnectionSetting
{
    public ConnectionPrefixes Prefix;
    public ConnectionTypes ConnectionType;
    public WeaverDataPoint Destination;
    public string Text;
}