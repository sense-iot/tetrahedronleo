using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class WeaverConnection : MonoBehaviour
{
    public WeaverDataPoint From, To;
    private LineRenderer lineRenderer;
    public Color color;
    public Color endColor;
    [SerializeField] private Transform coneTransform;
    [SerializeField] private float coneDist = 1;
    [SerializeField] private TextMesh textMesh;
    public string text = "";

    void Update()
    {
        textMesh.text = text;

        if (lineRenderer == null)
            lineRenderer = GetComponent<LineRenderer>();

        lineRenderer.SetColors(color, endColor);
        coneTransform.gameObject.GetComponentInChildren<Renderer>().material.color = color;

        coneTransform.position = Vector3.Lerp(From.transform.position, To.transform.position, 0.99999f);

        coneTransform.LookAt(To.transform.position);
        coneTransform.position -= coneTransform.forward * coneDist;

        lineRenderer.SetPositions(new Vector3[] { From.transform.position, coneTransform.position });

        textMesh.transform.position = Vector3.Lerp(From.transform.position, To.transform.position, 0.5f);
        textMesh.transform.LookAt(transform.position + (transform.position - Camera.main.transform.position), Camera.main.transform.up);
    }
}
