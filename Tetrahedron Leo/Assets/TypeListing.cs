//Volgorde NIET veranderen!
//Nieuwe elementen ALTIJD aan het einde van een lijst toevoegen
//Anders moet je alle bollen en verbindingen opnieuw instellen

public enum ConnectionPrefixes
{
    iim
}

public enum NodeTypes
{
    Objective,
    Activity,
    SpatialLocation,
    Concept,
    Environment,
    DesignedFunctionalPhysicalObject,
    ActualMaterializedPhysicalObject,
    PhysicalObject,
    InformationObject,
    Capability,
    StateOfIndividual,
    DesignedMaterializedPhysicalObject
}

public enum ConnectionTypes
{
    contributesTo,
    isRealizedBy,
    isLocatedAt,
    isRepresentedBy,
    possesses,
    hasTemporalPart,
    implements,
    isConstrainedBy,
    isExperiencedBy,
    isInstalledAs
}
